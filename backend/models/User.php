<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 * @property int $id
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string $verification_token
 * @property double $age
 * @property int $country
 * @property int $sex
 * @property int $city
 * @property int $city_type
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $source
 * @property string $birthday
 *
 * @property Datum[] $data
 */
class User extends \common\models\User
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['status', 'created_at', 'updated_at', 'country', 'sex', 'city_type'], 'integer'],
            [['age'], 'number'],
            [['birthday'], 'safe'],
            [['password_hash', 'password_reset_token', 'email', 'verification_token', 'first_name', 'middle_name', 'last_name'], 'string', 'max' => 255],
            [['auth_key', 'source'], 'string', 'max' => 32],
            [['city'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'verification_token' => 'Verification Token',
            'age' => 'Age',
            'country' => 'Country',
            'sex' => 'Sex',
            'city' => 'City',
            'city_type' => 'City Type',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'birthday' => 'Birthday',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getData()
    {
        return $this->hasMany(Datum::className(), ['user_id' => 'id']);
    }

    public static function getSexName()
    {
        return [
            1 => 'male',
            2 => 'female',
            3 => 'other'
        ];
    }
    public static function getCityType()
    {
        return [
            0 => '',
            1 => 'up to 10 thousand people',
            2 => 'up to 1 million people',
            3 => 'more than 10 million people'
        ];
    }
}
