<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contact_us".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $job
 * @property string $company
 * @property string $phone
 * @property string $country
 */
class ContactUs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact_us';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name','last_name', 'email'], 'required'],
            [['first_name', 'last_name', 'email', 'job', 'company', 'phone', 'country'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email Address',
            'job' => 'Job Title',
            'company' => 'Company Name',
            'phone' => 'Phone Number',
            'country' => 'Country',
        ];
    }
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom($email)
            ->setSubject('Message:')
            ->setHtmlBody('<p>First Name: '.$this->first_name.'</p>'.
                '<p>Last Name: '.$this->last_name.'</p>'.
                '<p>Company Name: '.$this->company.'</p>'.
                '<p>Job Title: '.$this->job.'</p>'.
                '<p>Country: '.$this->country.'</p>'.
                '<p>Phone Number: '.$this->phone.'</p>'
            )
            ->send();
    }
}
