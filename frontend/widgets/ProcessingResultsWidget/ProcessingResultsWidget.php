<?php
namespace frontend\widgets\ProcessingResultsWidget;


use yii\base\Widget;

class ProcessingResultsWidget extends Widget
{
    public function run()
    {
        return $this->render('default', ['age' => \Yii::$app->session->get('age'), ]);
    }
}