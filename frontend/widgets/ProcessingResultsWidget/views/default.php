<?php
/** @var $age */
/** @var $messages */
$messages = [
        'Are you allowed to use Internet without your parents supervision?',
        'Two words to describe you — young and beautiful!',
        'It\'s your time to run for President! Don\'t blow it.',
        'Are you allowed to use Internet without your kids\' supervision?',
];

if ($age < 90) $index = 3;
if ($age < 50) $index = 2;
if ($age < 35) $index = 1;
if ($age < 10) $index = 0;

$message  = $messages[$index];
?>
<style>
    table.blueTable {
        background-color: #FFE6F4;
        width: 60%;
        margin: auto;
        text-align: center;
        border: 5px solid white;
        border-radius: 7px !important;
        border-collapse: separate !important;
    }
    table.blueTable td, table.blueTable th {
        padding: 3px 2px;
    }
    table.blueTable tbody td {
        font-size: 13px;
    }
    table.blueTable tr:nth-child(even) {
        background: #FFD2D2;
    }
    table.blueTable thead {
        background: #FF7474;
        background: -moz-linear-gradient(top, #ff9797 0%, #ff8282 66%, #FF7474 100%);
        background: -webkit-linear-gradient(top, #ff9797 0%, #ff8282 66%, #FF7474 100%);
        background: linear-gradient(to bottom, #ff9797 0%, #ff8282 66%, #FF7474 100%);
    }
    table.blueTable thead th {
        font-size: 15px;
        font-weight: bold;
        color: #FFFFFF;
        border-left: 2px solid #FFE1DA;
    }
    table.blueTable thead th:first-child {
        border-left: none;
    }

    table.blueTable tfoot td {
        font-size: 14px;
    }
    table.blueTable tfoot .links {
        text-align: right;
    }
</style>
<table class="blueTable hidden">
    <thead>
    <tr>
        <th>head1</th>
        <th>head2</th>
        <th>head3</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>cell1_1</td>
        <td>cell2_1</td>
        <td>cell3_1</td>
    </tr>
    <tr>
        <td>cell1_2</td>
        <td>cell2_2</td>
        <td>cell3_2</td>
    </tr>
    <tr>
        <td>cell1_3</td>
        <td>cell2_3</td>
        <td>cell3_3</td>
    </tr>
    <tr>
        <td>cell1_4</td>
        <td>cell2_4</td>
        <td>cell3_4</td>
    </tr>
    </tbody>
</table>

<p class="text-center"><?= $message; ?></p>
