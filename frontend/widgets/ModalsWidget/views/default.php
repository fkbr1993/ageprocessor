<?php
use \yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->registerJsFile('/js/RecordRTC.js');
$this->registerJsFile('/js/waveform.js');
?>

<div id="ex1" class="modal feauture-is-under-development">
    <div class="voice_modal_bg1_future">
        <div class="voice_modal_bg1" >
            <div class="voice_modal_bg2 ">

            </div>
        </div>
    </div>
    <div class="modal_content">
        <div class="question-content">
            <h3 class="f_title_h3 title-modal text-center" style="">Feature is under development</h3>
            <p class="text-working" style="text-align: center">We are working on it intensive and expect to finish soon.<br>
                Please left us your email, so we can notify when we ready.</p>
            <div class="notify-me-wrapper">
                <div class="notify-me">

                    <?php $form = ActiveForm::begin([
                        'id' => 'notify-me-form',
                        #'options' => ['class' => 'form-horizontal'],
                    ]) ?>

                    <label class="hidden">Email</label>
                    <input class="btn-pink-mini" placeholder="Your Email Here" name="email">
                    <button class="btn-pink-mini send-email"
                            onclick="ga('send', 'event', 'AdvancedFeature', 'ShareMoreData', 'phone-number-middle')">
 <span class="pink-title-mini quit-modal_" rel="modal:close" href="#close-modal">
                           Notify me
                       </span>
                    </button>

                    <?php ActiveForm::end() ?>

                </div>
            </div>
        </div>        <a class=" quit-modal " rel="modal:close" href="#close-modal">< QUIT</a>


    </div>

</div>


<div id="ex2" class="modal">
    <div class="voice_modal_bg1">
        <div class="voice_modal_bg2 ">
            <span class="voice_title">Voice recording</span>
            <div class="modal_content">
                <div class="question-content">
                    <h3 class="f_title_h3 title-modal" style="">Read the text</h3>
                    <h4 class="age-result-text hidden" style="color: black">Your age: <span class="age-result">0</span></h4>
                    <p class="text-grey modal-text">Я очень любил фильмы про лётчиков; с одним из таких фильмов и было связано
                        сильнейшее переживание
                        моего детства. Однажды, в космически чёрный декабрьский вечер, я включил тёткин телевизор и
                        увидел на его экране
                        покачивающий крыльями самолёт с пиковым тузом и крестом на фюзеляже.

                    </p>
                    <div class="data-holder" style="display:none; margin-top: -25px"></div>


                </div>

            </div>

        </div>

        <div class="loader" style="display:none;"></div>
        <div class="snap">
            <div class="btn-start-recording">
                <img class="" src="/img/elements/snap.svg" id="btn-start-recording">
                <p style="padding: 0px 12px;">Start</p>
            </div>

            <div class="btn-stop-recording" style="display: none;     margin-left: 10px;
    margin-top: 10px;">
                <img class="stop-recording" src="/img/elements/stop.png" id="btn-stop-recording">
                <p style="margin: 0px 12px">Stop</p>
            </div>

            <button id="btn-release-microphone" disabled="" style="display: none;">Release Microphone</button>
            <div style="display: none">
                <audio controls="" autoplay="" playsinline=""></audio>
            </div>

        </div>

        <a class=" quit-modal " rel="modal:close" href="#close-modal">< QUIT</a>

    </div>

</div>

<div id="ex3" class="modal take-a-photo">
    <img class="upload_image" src="/img/elements/image 2.jpg">
    <span class="upload_title">Take a photo</span>
    <img class="snap" src="/img/elements/snap.svg">
</div>
<script>
    window.ga = function () {
        ga.q.push(arguments)
    };
    ga.q = [];
    ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto');
    ga('set', 'transport', 'beacon');
    ga('send', 'pageview')

</script>
<script src="https://www.google-analytics.com/analytics.js" async></script>