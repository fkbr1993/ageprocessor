<?php
namespace frontend\widgets\ModalsWidget;


use yii\base\Widget;

class ModalsWidget extends Widget
{
    public function run()
    {
        return $this->render('default', []);
    }
}