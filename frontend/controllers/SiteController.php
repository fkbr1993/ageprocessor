<?php

namespace frontend\controllers;

use app\models\Data;
use common\models\ContactUs;
use frontend\models\Country;
use frontend\models\forms\AnalyzeForm;
use frontend\models\forms\VoiceForm;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use yii\authclient\AuthAction;
use yii\base\InvalidArgumentException;
use frontend\models\Auth;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use app\models\User;
use frontend\widgets\ProcessingResultsWidget\ProcessingResultsWidget;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'save-voice' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => AuthAction::class,
                'successCallback' => [$this, 'successCallback']
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionTest()
    {
        return $this->render('test', []);
    }

    public function actionNotify()
    {

    }

    public function actionSaveVoice()
    {
        $model = new VoiceForm();

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {

            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->validate()) {
                $model->file->saveAs('uploads/' . $model->filename);
            }
        }

        //TODO: extract features

        return Json::encode([
            'data' => $_POST,
            'html' => ProcessingResultsWidget::widget([]),
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new AnalyzeForm();

        $model->load(Yii::$app->request->get());

        return $this->render('index', ['model' => $model,]);
    }

    /**
     * Displays workspace.
     *
     * @return mixed
     */
    public function actionWorkspace()
    {
        return $this->render('workspace');
    }

    /**
     * Displays privacy-policy.
     *
     * @return mixed
     */
    public function actionPrivacyPolicy()
    {
        return $this->render('privacy-policy');
    }

    /**
     * Displays result page.
     *
     * @return mixed
     */
    public function actionResult()
    {
        $model = new AnalyzeForm();

        $model->load(Yii::$app->request->get());

        return $this->render('result', ['model' => $model,]);
    }


    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/');
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactUs();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail']) && $model->save()) {
                Yii::$app->session->setFlash('success', "Thank
                                        you! Your message has been sent. We'll get back to you as soon as possible.");
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }
            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * user Data upload page.
     *
     * @return mixed
     */
    public function actionAddData()
    {
        if (!Yii::$app->user->isGuest) {

            $model = new Data();
            if ($model->load(Yii::$app->request->post())) {
                $model->uploadFile = UploadedFile::getInstance($model, 'uploadFile');
                $fileName = rand(0, 99) . $model->uploadFile;


                $model->file = $fileName;
                $model->user_id = Yii::$app->getUser()->identity->getId();
                $model->created_at = time();
                $model->updated_at = time();
                $model->raw_data = '';
                $model->partner_id = 0;
                $model->uploadFile->saveAs(Yii::getAlias('@webroot') . '/uploads/' . $fileName);
                $model->save(false);
                if ($model->uploadFile->extension == 'xls' || $model->uploadFile->extension == 'xlsx') {
                    $objPHPExcel = \PHPExcel_IOFactory::load(Yii::getAlias('@webroot') . '/uploads/' . $model->file);
//                echo "<pre>";
//                print_r($objPHPExcel->getActiveSheet()->toArray(null, true, true, true));
                    $data = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    $model->raw_data = json_encode($data);
                    $model->save(false);
                } else {
                    $data = file_get_contents(Yii::getAlias('@webroot') . '/uploads/' . $model->file);
                    $model->raw_data = json_encode($data);
                    $model->save(false);
                }

            }
            $model->uploadFile = '';

            $files = $model->find()->where(['user_id' => Yii::$app->getUser()->identity->getId()])->all();
            return $this->render('add_data', [
                'model' => $model,
                'files' => $files
            ]);
        }
    }

    public function actionDeleteFile()
    {
        $id = Yii::$app->request->get('id');
        $model = Data::findOne($id);
        if (file_exists(Yii::getAlias('@webroot') . '/uploads/' . $model->file)) {
            unlink(Yii::getAlias('@webroot') . '/uploads/' . $model->file);
        }
        $model->delete();
        return $this->redirect('add-data');
    }

    /**
     * user Profile page.
     *
     * @return mixed
     */
    public function actionProfile()
    {
        if (!Yii::$app->user->isGuest) {
            $model = User::findOne(Yii::$app->user->identity->getId());
            $countries = Country::find()->all();
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $model->birthday = date("Y-m-d", strtotime($model->birthday));
                $model->city = ucfirst($model->city);
                $model->save();
                Yii::$app->session->setFlash('success', 'Update user data');
            }
            $model->birthday = date('d-m-Y', strtotime($model->birthday));
            return $this->render('profile', [
                'model' => $model,
                'countries' => $countries
            ]);
        } else {
            return $this->goHome();
        }
    }

    public function successCallback($client)
    {
        $attributes = $client->getUserAttributes();

        $auth = User::find()->where(['email' => $attributes['email'], 'source' => $attributes['id']])->one();
        if (Yii::$app->user->isGuest) {
            if (!empty($auth) && Yii::$app->user->login($auth)) { // login
                $this->redirect(['site/profile']);
            } else { // signup
                if (User::find()->where(['email' => $attributes['email']])->exists()) {
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", ['client' => $client->getTitle()]),
                    ]);
                } else {
                    $password = Yii::$app->security->generateRandomString(6);
                    if (isset($attributes['name']['givenName']) && isset($attributes['name']['familyName'])) {
                        $user = new User([
                            'first_name' => $attributes['name']['givenName'],
                            'last_name' => $attributes['name']['familyName'],
                            'email' => $attributes['emails'][0]['value'],
                            'password_hash' => $password,
                            'source' => $attributes['id'],
                            'created_at' => time(),
                            'updated_at' => time()
                        ]);
                    } else {
                        if (!isset($attributes['email'])) {
                            Yii::$app->getSession()->setFlash('error', [
                                Yii::t('app', "Err Do not exists email in your account!", ['client' => $client->getTitle()]),
                            ]);
                        } else {
                            $user = new User([
                                'first_name' => $attributes['first_name'],
                                'last_name' => $attributes['last_name'],
                                'email' => $attributes['email'],
                                'password_hash' => $password,
                                'source' => $attributes['id'],
                                'created_at' => time(),
                                'updated_at' => time()
                            ]);
                        }

                    }

                    $user->generateAuthKey();
                    $user->generatePasswordResetToken();

                    if ($user->save(false) && Yii::$app->user->login($user)) {
                        Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
                        $this->redirect(['site/profile']);

                    } else {
                        print_r($user->getErrors());
                    }
                }
            }
        } else { // user already logged in

        }

    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }


        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @return yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    public function actionRunPython()
    {
        ob_start();
        passthru('/usr/bin/python2.7 /home/f/fkbr1993/mvp-lifetracker/ModelExecutor.py -i file.wav -a age;');
        $output = ob_get_clean();
        Yii::$app->session->set('age', $output);
        return $output;
    }

}
