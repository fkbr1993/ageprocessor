$(document).ready(function () {
    // progressbar.js@1.0.0 version is used
// Docs: http://progressbarjs.readthedocs.org/en/1.0.0/
    if($('#progress1').length){
        var bar = new ProgressBar.Circle(progress1, {
            color: '#33333',
            // This has to be the same size as the maximum width to
            // prevent clipping
            strokeWidth: 8,
            trailWidth: 8,
            easing: 'easeInOut',
            duration: 3000,
            strokeLinecap: 'round',
            text: {
                autoStyleContainer: false
            },
            from: {color: '#FF84AC', width: 8},
            to: {color: '#FF84AC', width: 8},
            // Set default step function for all animate calls
            step: function (state, circle) {
                circle.path.setAttribute('stroke', state.color);
                circle.path.setAttribute('stroke-width', state.width);
                circle.path.setAttribute('stroke-linecap', 'round');

                var value = Math.round(circle.value() * 100);
                if (value === 0) {
                    circle.setText('');
                } else {
                    circle.setText(value);
                }

            }
        });
        bar.text.style.fontFamily = 'Circe';
        bar.text.style.fontSize = '36px';

        bar.text.style.fontS = 'bo';
        bar.text.style.fontWeight = 'bold';
        bar.animate(0.9);
        var bar2 = new ProgressBar.Circle(progress2, {
            color: '#33333',
            // This has to be the same size as the maximum width to
            // prevent clipping
            strokeWidth: 8,
            trailWidth: 8,
            easing: 'easeInOut',
            duration: 3000,
            strokeLinecap: 'round',
            text: {
                autoStyleContainer: false
            },
            from: {color: '#FF95E8', width: 8},
            to: {color: '#FF95E8', width: 8},
            // Set default step function for all animate calls
            step: function (state, circle) {
                circle.path.setAttribute('stroke', state.color);
                circle.path.setAttribute('stroke-width', state.width);
                circle.path.setAttribute('stroke-linecap', 'round');

                var value = Math.round(circle.value() * 100);
                if (value === 0) {
                    circle.setText('');
                } else {
                    circle.setText(value);
                }

            }
        });
        bar2.text.style.fontFamily = 'Circe';
        bar2.text.style.fontSize = '36px';

        bar2.text.style.fontS = 'bo';
        bar2.text.style.fontWeight = 'bold';
        bar2.animate(0.5);  // Number from 0.0 to 1.0
    }

})