$(document).ready(function () {
    Select.init({className: 'select-theme-chosen'});
    // if ($(window).width() > 760) {
    //     $.scrollify({
    //         section: ".section",
    //         sectionName: "section",
    //         interstitialSection: "",
    //         easing: "easeOutExpo",
    //         scrollSpeed: 1100,
    //         scrollbars: true,
    //         standardScrollElements: "",
    //         overflowScroll: true,
    //
    //         afterResize: function () {
    //         },
    //         afterRender: function () {
    //         }
    //     });
    // }

//methods

    wow = new WOW(
        {
            boxClass: 'wow',      // default
            animateClass: 'animated', // default
            offset: 0,          // default
            mobile: true,       // default
            live: true        // default
        }
    )
    wow.init();
//    animations
    $(document).on('click','.send-email',function () {
        $('.quit-modal').trigger('click');
        /*$.post( "/site/notify", { name: "John", time: "2pm" }, function (data) {
            console.log(data)
        } );*/
    });
    setInterval(function () {
        $('.scrool-figure-2').css("margin-top", "2px");
        setTimeout(function () {
            $(".scrool-figure-2").animate({"margin-top": "24px"});
        }, 1000);
    }, 3000);
    //
    // var ice = $('.our-team-section');
    // function anim() {
    //     setInterval(function(){
    //         ice.animate({
    //                 top:'+=20'},
    //             { duration:3000,
    //                 step:function()
    //                 {}
    //             });
    //
    //
    //         ice.animate({
    //                 top:'-=20'},
    //             { duration:3000,
    //                 step:function()
    //                 { }
    //             });
    //     });
    // }

    // anim();
    $('button[data-target="navMobile"]').click(function () {
        var id = $(this).attr('data-target');
        if (!$('#' + id).hasClass('show')) {
            $('#' + id).addClass('in');
            $('#' + id).addClass('show');
            $('#' + id).animate({
                    height: '200'
                },
                {
                    duration: 800,
                    step: function () {
                    }
                });
        } else {
            $('#' + id).animate({
                    height: '0'
                },
                {
                    duration: 800,
                    step: function () {
                        $('#' + id).removeClass('show');
                    }
                });
        }


    })

});
// Counter
$(document).ready(function () {

    var counters = $(".count");
    var countersQuantity = counters.length;
    var counter = [];

    for (i = 0; i < countersQuantity; i++) {
        counter[i] = parseInt(counters[i].innerHTML);
    }

    var count = function (start, value, id) {
        var localStart = start;
        setInterval(function () {
            if (localStart < value) {
                localStart++;
                counters[id].innerHTML = localStart;
            }
        }, 40);
    }

    for (j = 0; j < countersQuantity; j++) {
        count(0, counter[j], j);
    }
});

