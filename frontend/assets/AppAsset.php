<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/normalize.css',
        'fonts/stylesheet.css',
        'css/odometer-theme-default.css',
        'css/main.css',
        'css/bootstrap.min.css',
        'css/animate.css',
        'css/step.css',
        'css/select-theme-chosen.css',
        'css/stepForm.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css',
    ];
    public $js = [
        'js/wow.min.js',
        'js/progressbar.min.js',
        'js/jquery.modal.min.js',
        'js/jquery.scrollify.js',
        'js/main.js',
        'js/easejq.js',
        'js/main_progressbar.js',
        'js/ager.js',
        'js/stepForm.js',
        'js/odometer.min.js',
        'js/tether.js',
        'js/select.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
