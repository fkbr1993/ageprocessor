<?php

/* @var $this yii\web\View */
/* @var $model \frontend\models\forms\AnalyzeForm */


$this->title = 'Lifetracker — first lifespan enhancing service';

use yii\authclient\widgets\AuthChoice; ?>
<div class="site-index">


    <div class="jumbotron">

        <?= $this->render('_resultForm', ['model' => $model, ])?>

        <p></p>

        <?= $this->render('_clarifyOne', ['model' => $model, ])?>
    </div>

    <div class="body-content hidden">

        <div class="row row-center">
            <div class="col-lg-4">
                <p>Free online-test</p>

                <!--<p>Bringing your idea to life is easier with the right tools. Deploy it into reality with a few lines of code.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>-->
            </div>
            <div class="col-lg-4">
                <p>Security first</p>

                <!--<p>The largest enterprises trust Twilio with their data. We’re ISO 27001 and GDPR Privacy Shield compliant.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>-->
            </div>
            <div class="col-lg-4">
                <p>99.999% Forecast Success Rate</p>

                <!--<p>Forecast success rate is the true indicator of your app experience. We won’t settle for errors that take down your app.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>-->
            </div>
        </div>

    </div>
</div>
