<?php

/* @var $this yii\web\View */
/* @var $model \frontend\models\forms\AnalyzeForm */


$this->title = 'Age Processor — first lifespan enhancing service';

use yii\authclient\widgets\AuthChoice;
use yii\web\JqueryAsset; ?>


    <!-- Footer Start -->
    <section id="section-1" class="container-fluid section" style="margin-bottom: -18px;">
        <div class="slider brk-morph-4 bubble">

        </div>
        <div class="text-slider">
            <div class="row">
                <div class="col-md-3 col-sm-2">

                </div>
                <div class="col-md-9 col-sm-10">
                    <p class="f_title_h1">Control your <span>age</span></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-0">

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <p class="f_title_h4" style="">Age less and live more</p>
                </div>
            </div>
            <div class="">
                <div class="row ">
                    <div class="col-md-1 col-sm-1">

                    </div>
                    <div class="col-md-10 col-sm-6 panel-container-10">
                        <div class="white_panel">
                            <div class="row">
                                <form action="" id="getAnaliz">
                                    <div class="col-md-3" style="margin-right: 20px">
                                        <label class="label-form">Country</label>
                                        <p>
                                            <select class="select-white-btn themes-select select-select select-theme-chosen">
                                                <option>Russian Federation</option>
                                                <option>USA</option>
                                                <option>China</option>
                                            </select>
                                        <p>
                                    </div>
                                    <div class="col-md-1 age-select">
                                        <label class="label-form">Age</label>
                                        <p>
                                            <select class="select-white-btn">
                                                <?php for ($i = 18; $i <= 99; $i++) { ?>
                                                    <option><?php echo $i; ?></option>
                                                <?php }?>
                                            </select>
                                        <p>
                                    </div>

                                    <div class="col-md-3">
                                        <label class="label-form">Sex</label>
                                        <p>
                                            <input id="toggle-on" class="toggle toggle-left" name="toggle"
                                                   value="true"
                                                   type="radio" checked>
                                            <label for="toggle-on" class="btn-left">Male</label>

                                            <input id="toggle-off" class="toggle toggle-right" name="toggle"

                                                   value="false"
                                                   type="radio">
                                            <label for="toggle-off" class="btn-right">Female</label>

                                        </p>
                                    </div>

                                </form>
                                <div class="col-md-4 text-center">
                                    <a href="/site/workspace">
                                        <button class="btn-pink"><span class="btn-pink-title">GET ANALYSIS</span>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-1">
                        <div class="pull-right">
                            <ul class="right-navigation">
                                <a class="scrool-to active" data-id="section-1">
                                    <li></li>
                                </a>
                                <a class="scrool-to" data-id="section-2">
                                    <li></li>
                                </a>
                                <a href="#" class="scrool-to" data-id="section-3">
                                    <li></li>
                                </a>
                                <a href="#" class="scrool-to" data-id="section-4">
                                    <li></li>
                                </a>
                                <a href="#" class="scrool-to" data-id="section-5">
                                    <li></li>
                                </a>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">

                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="col-md-5">

                    </div>
                </div>
            </div>

        </div>
    </section>
</div>
<br>
<!--    Section 2    -->
<section class="section" data-section-name="section" style="margin-top: 100px">
    <div class="container-fluid">
        <div class="row section-2" id="section-2">
            <div class="col-md-5 col-sm-1">

            </div>
            <div class="col-md-6">
                <p class="f_title_h2 title-center">How does it <span>work</span> ? </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">

            </div>
            <div class="col-md-10 panel-container-10">
                <div class="col-md-3 col-sm-6 text-center">
                    <img class="img-process wow zoomIn " data-wow-duration="1s" data-wow-delay="0.2s"
                         src="/img/elements/upload_img.svg">
                    <h3 class="text-black">Upload photo</h3>
                    <p class="text-grey">Our service need to know, who are you today</p>
                </div>
                <div class="col-md-3 col-sm-6 text-center">
                    <img class="img-process wow  zoomIn " data-wow-duration="1s" data-wow-delay="0.5s"
                         src="/img/elements/recover_voice.svg">
                    <h3 class="text-black">Record voice</h3>
                    <p class="text-grey">We collect your special data to provide you unique and explicit
                        results</p>
                </div>
                <div class="col-md-3 col-sm-6 text-center">
                    <img class="img-process wow zoomIn delay-3s" data-wow-duration="1s" data-wow-delay="0.8s"
                         src="/img/elements/analiz.png">
                    <h3 class="text-black">Complete a simple test</h3>
                    <p class="text-grey">Tell us about your habits and lifestile</p>
                </div>
                <div class="col-md-3 col-sm-6 text-center">
                    <img class="img-process wow zoomIn delay-4s" data-wow-duration="1s" data-wow-delay="1s"
                         src="/img/elements/complate_simple.png">
                    <h3 class="text-black">Analyze results</h3>
                    <p class="text-grey">Our trained brain AgeScan will provide you best practices to improve
                        your
                        longevity</p>
                </div>
            </div>
            <div class="col-md-1">

            </div>
        </div>
        <div class="row">
            <div class="col-md-4">

            </div>
            <div class="col-md-4 text-center btn-top">
                <a href="/site/workspace">
                <button class="btn-pink"><span class="btn-pink-title">GET ANALYSIS</span></button>
                </a>
            </div>
            <div class="col-md-4">

            </div>
        </div>

    </div>
</section>
<section id="section-3" class="section ">
    <div class="container-fluid hidden">
        <div class="row wow fadeInUp">
            <div class="col-md-1 col-sm-1">

            </div>
            <div class="col-md-7 col-sm-10 text-center-table">
                <h2 class="f_title_h2">Mission</h2>
                <p class="text-black-h4">We help increase life expectancy using recent scientific achievements
                    and
                    Artificial Intelligence.</p>
            </div>
            <div class="col-md-2">

            </div>
        </div>
        <div class="row  title-values-mobil text-center-table" style="">
            <h2 class="f_title_h2">Values</h2>
            <p class="text-black-h4" style="">What is really important for us</p>
        </div>
        <div class="row" style="margin-top: 100px">
            <div class="col-md-1">

            </div>
            <div class="ellipse col-md-8">
                <div class="row">
                    <div class="col-md-2 col-sm-2" style="margin-right:30px">

                    </div>


                    <div class="col-md-7 col-sm-12" style="margin-top: 65px; ">
                        <div class="white_panel table-card">
                            <h3 class="text-black-24">Scientific Approach</h3>
                            <p class="text-grey-16">A large layer of scientific data and knowledge accumulated
                                by
                                biology and medicine over
                                the past decades is the fuel of Age processor. We are a team of scientists,
                                therefore,
                                we value scientific validity.</p>
                        </div>
                    </div>
                    <div class="col-md-2">

                    </div>

                </div>
                <div class="row">

                    <div class="col-md-6 col-sm-12">
                        <div class="white_panel table-card">
                            <h3 class="text-black-24">Broadly Distributed Benefits</h3>
                            <p class="text-grey-16">We will use the achievements of science and artificial
                                intelligence
                                for
                                the common good. Everyone should have free access to the main advantages of our
                                service
                                - this is one of the most important principles of our work.</p>
                        </div>
                    </div>


                    <div class="col-md-6 col-sm-12">
                        <div class="white_panel table-card" style="margin-top: 20px">
                            <h3 class="text-black-24">Technical Leadership</h3>
                            <p class="text-grey-16">To be effective in utilizing the knowledge and achievements
                                of longevity for society, we need to be at the forefront of the capabilities of
                                artificial intelligence. The volume of accumulated data is constantly growing
                                and is already too large for</p>
                        </div>
                    </div>

                </div>

            </div>
            <div class="col-md-3  title-values" style="">
                <h2 class="f_title_h2">Values</h2>
                <p class="text-black-h4" style="width: 400px;">What is really important for us</p>
            </div>
        </div>

    </div>
</section>

<!--  Section Our Teaam  -->
<!--<section id="section-4" class="section">-->
<!--    <div class="our-team-section brk-morph-1 bubble margin-top" >-->
<!--    </div>-->
<!--    <div class="container section2-content">-->
<!--        <div class="row margin-section" style="">-->
<!--            <div class="col-md-1">-->
<!--            </div>-->
<!--            <div class="col-md-6 col-sm-6" >-->
<!---->
<!--                <p class="f_title_h2">Our <span>team</span></p>-->
<!--                <p class="title-lg">We try to do our best to provide you spectacular product</p>-->
<!--            </div>-->
<!--            <div class="col-md-5 col-sm-12 col-xs-12">-->
<!--                <div class="card pull-right ">-->
<!--                    <div class="front white-box-cart ">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-4  col-xs-3 col-sm-3">-->
<!--                                <img class="avatar" style="" src="/img/elements/avatar.svg">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-xs-9 col-sm-9 text-center" style="padding: 40px">-->
<!--                                <p class="person-name">Emily Bloom</p>-->
<!--                                <p class="text-pink">Co-founder</p>-->
<!--                                <p class="text-grey">Suttitile</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="back white-box-cart">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-4 col-xs-3 col-sm-3">-->
<!--                                <img class="avatar" src="/img/elements/avatar.svg">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-xs-9 col-sm-9 text-center" style="padding: 37px 25px;">-->
<!--                                <p class="person-name">Emily Bloom</p>-->
<!--                                <p class="text-pink">Co-founder</p>-->
<!--                                <p class="text-grey">+79929943434</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row card-break" style="">-->
<!--            <div class="col-lg-1">-->
<!---->
<!--            </div>-->
<!--            <div class="col-md-5 col-sm-6 col-xs-12 second-card">-->
<!--                <div class="card">-->
<!--                    <div class="front white-box-cart">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-4 col-xs-3 col-sm-3">-->
<!--                                <img class="avatar" style="" src="/img/elements/avatar.svg">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-xs-9 col-sm-9 text-center" style="padding: 40px">-->
<!--                                <p class="person-name">Emily Bloom</p>-->
<!--                                <p class="text-pink">Co-founder</p>-->
<!--                                <p class="text-grey">Suttitile</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="back white-box-cart">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-4 col-xs-3 col-sm-3">-->
<!--                                <img class="avatar" src="/img/elements/avatar.svg">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-xs-9 col-sm-9 text-center" style="padding: 37px 25px;">-->
<!--                                <p class="person-name">Emily Bloom</p>-->
<!--                                <p class="text-pink">Co-founder</p>-->
<!--                                <p class="text-grey">+79929943434</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-5 col-sm-6 col-xs-12">-->
<!--                <div class="card">-->
<!--                    <div class="front white-box-cart">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-4 col-xs-3 col-sm-3">-->
<!--                                <img class="avatar" style="" src="/img/elements/avatar.svg">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-xs-9 col-sm-9 text-center" style="padding: 40px">-->
<!--                                <p class="person-name">Emily Bloom</p>-->
<!--                                <p class="text-pink">Co-founder</p>-->
<!--                                <p class="text-grey">Suttitile</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="back white-box-cart">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-4 col-xs-3 col-sm-3">-->
<!--                                <img class="avatar" src="/img/elements/avatar.svg">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-xs-9 col-sm-9 text-center" style="padding: 37px 25px;">-->
<!--                                <p class="person-name">Emily Bloom</p>-->
<!--                                <p class="text-pink">Co-founder</p>-->
<!--                                <p class="text-grey">+79929943434</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-lg-1">-->
<!---->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row card-break" style="">-->
<!--            <div class="col-lg-2">-->
<!---->
<!--            </div>-->
<!--            <div class="col-md-5 col-sm-6 col-xs-12 thrid-card">-->
<!--                <div class="card">-->
<!--                    <div class="front white-box-cart">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-4 col-xs-3 col-sm-3">-->
<!--                                <img class="avatar" style="" src="/img/elements/avatar.svg">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-xs-9 col-sm-9 text-center" style="padding: 40px">-->
<!--                                <p class="person-name">Emily Bloom</p>-->
<!--                                <p class="text-pink">Co-founder</p>-->
<!--                                <p class="text-grey">Suttitile</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="back white-box-cart">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-4 col-xs-3 col-sm-3">-->
<!--                                <img class="avatar" src="/img/elements/avatar.svg">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-xs-9 col-sm-9 text-center" style="padding: 37px 25px;">-->
<!--                                <p class="person-name">Emily Bloom</p>-->
<!--                                <p class="text-pink">Co-founder</p>-->
<!--                                <p class="text-grey">+79929943434</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-5 col-sm-6 col-xs-12">-->
<!--                <div class="card">-->
<!--                    <div class="front white-box-cart">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-4 col-xs-3 col-sm-3">-->
<!--                                <img class="avatar" style="" src="/img/elements/avatar.svg">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-xs-9 col-sm-9 text-center" style="padding: 40px">-->
<!--                                <p class="person-name">Emily Bloom</p>-->
<!--                                <p class="text-pink">Co-founder</p>-->
<!--                                <p class="text-grey">Suttitile</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="back white-box-cart">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-4 col-xs-3 col-sm-3">-->
<!--                                <img class="avatar" src="/img/elements/avatar.svg">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-xs-9 col-sm-9 text-center" style="padding: 37px 25px;">-->
<!--                                <p class="person-name">Emily Bloom</p>-->
<!--                                <p class="text-pink">Co-founder</p>-->
<!--                                <p class="text-grey">+79929943434</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->
<!--  Section Our Teaam end -->
<!--  Section Press Review  -->
<!--<section id="section-5" class="section">-->
<!--    <div class="review-section">-->
<!--        <div class="container desctop-section">-->
<!--            <div class="row" style="">-->
<!--                <div class="col-md-8 press-value-mobil text-center-table">-->
<!--                    <p class="f_title_h2 ">Press <span>reviews</span></p>-->
<!--                    <p class="text-black-h4 ">Fcol-amous journalists about our service</p>-->
<!--                </div>-->
<!--                <div class="col-md-4 col-xs-12">-->
<!--                    <div class="figure-box-1">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-3 col-xs-3 ">-->
<!--                                <img class="figure-box-img1" src="/img/elements/circl-avatar2.png">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-sm-8 content-figure-1" style="">-->
<!--                                <p class="comment-text">"Lorem ipsum dolor sit amet, consectetur adipisicing-->
<!--                                    elit."-->
<!--                                </p>-->
<!--                                <p class="text-grey">Co-founder</p>-->
<!--                                <p class="text-pink">Read article</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-8 col-xs-12 press-div" >-->
<!--                    <div class="col-md-4">-->
<!---->
<!--                    </div>-->
<!--                    <div class="col-md-8 press-value">-->
<!--                        <p class="f_title_h2 ">Press <span>reviews</span></p>-->
<!--                        <p class="text-black-h4 ">Fcol-amous journalists about our service</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row" style="">-->
<!--                <div class="col-md-6 col-sm-6">-->
<!--                    <div class="figure-box-3" style="">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-3 col-xs-3">-->
<!--                                <img class="figure-box-img2" style="    margin-top: 77px;-->
<!--    margin-left: 2px;" src="/img/elements/circl-avatar3.png">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-xs-8" style=" padding: 40px;   margin-top: 39px;">-->
<!--                                <p class="comment-text">"Lorem ipsum dolor sit amet, consectetur adipisicing-->
<!--                                    elit."-->
<!--                                </p>-->
<!--                                <p class="text-grey">Co-founder</p>-->
<!--                                <p class="text-pink">Read article</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-6 col-sm-6">-->
<!--                    <div class="figure-box-2" style="">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-3 col-xs-3">-->
<!--                                <img class="figure-box-img2" style="margin-top: 80px;-->
<!--    margin-left: -14px;" src="/img/elements/circl-avatar1.svg">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-xs-8 position-av3" style="padding: 40px; margin-top: 40px;">-->
<!--                                <p class="comment-text">"Lorem ipsum dolor sit amet, consectetur adipisicing-->
<!--                                    elit."-->
<!--                                </p>-->
<!--                                <p class="text-grey">Co-founder</p>-->
<!--                                <p class="text-pink">Read article</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!---->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="container table-section">-->
<!--            <div class="row" style="">-->
<!--                <div class="col-md-8 press-value-mobil text-center-table">-->
<!--                    <p class="f_title_h2 ">Press <span>reviews</span></p>-->
<!--                    <p class="text-black-h4 ">Fcol-amous journalists about our service</p>-->
<!--                </div>-->
<!---->
<!--                <div class="col-md-8 col-xs-12 press-div" style="">-->
<!--                    <div class="col-md-4">-->
<!---->
<!--                    </div>-->
<!--                    <div class="col-md-8 press-value">-->
<!--                        <p class="f_title_h2 ">Press <span>reviews</span></p>-->
<!--                        <p class="text-black-h4 ">Fcol-amous journalists about our service</p>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row" style="">-->
<!--                <div class="col-md-6 col-sm-6">-->
<!--                    <div class="figure-box-1">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-3 col-xs-3 ">-->
<!--                                <img class="figure-box-img1 " src="/img/elements/circl-avatar2.png">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-sm-8 content-figure-1" style="">-->
<!--                                <p class="comment-text">"Lorem ipsum dolor sit amet, consectetur adipisicing-->
<!--                                    elit."-->
<!--                                </p>-->
<!--                                <p class="text-grey">Co-founder</p>-->
<!--                                <p class="text-pink">Read article</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-6 col-sm-6">-->
<!--                    <div class="figure-box-3" style="">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-3 col-xs-3">-->
<!--                                <img class="figure-box-img2" style=" " src="/img/elements/circl-avatar3.png">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-xs-8" style=" padding: 40px;   margin-top: 39px;">-->
<!--                                <p class="comment-text">"Lorem ipsum dolor sit amet, consectetur adipisicing-->
<!--                                    elit."-->
<!--                                </p>-->
<!--                                <p class="text-grey">Co-founder</p>-->
<!--                                <p class="text-pink">Read article</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-md-6 col-sm-10 col-xs-10 pull-right">-->
<!--                    <div class="figure-box-2" style="">-->
<!--                        <div class="row">-->
<!--                            <div class="col-md-3 col-xs-3">-->
<!--                                <img class="figure-box-img3" style="margin-top: 80px;-->
<!--    margin-left: -14px;" src="/img/elements/circl-avatar1.svg">-->
<!--                            </div>-->
<!--                            <div class="col-md-8 col-xs-8" style="padding: 40px; margin-top: 40px;">-->
<!--                                <p class="comment-text">"Lorem ipsum dolor sit amet, consectetur adipisicing-->
<!--                                    elit."-->
<!--                                </p>-->
<!--                                <p class="text-grey">Co-founder</p>-->
<!--                                <p class="text-pink">Read article</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!---->
<!---->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->
<!--  Section Press review end -->
<!-- Footer End -->
<div class="bottom ">

</div>
<script>
    window.ga = function () {
        ga.q.push(arguments)
    };
    ga.q = [];
    ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto');
    ga('set', 'transport', 'beacon');
    ga('send', 'pageview')

</script>
<script src="https://www.google-analytics.com/analytics.js" async></script>