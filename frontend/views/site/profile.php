<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$countries = ArrayHelper::map($countries, 'id', 'name_en');
?>

<div class="user-form">
    <h3>Profile</h3>
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">

        <div class="col-md-3">
            <?= $form->field($model, 'birthday')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter birth date ...'],
                'value' => date('dd-mm-yyyy',strtotime($model->birthday)),
                'pluginOptions' => [
                    'format' => 'dd-mm-yyyy',
                    'todayHighlight' => true,
                    'autoclose' => true
                ]
            ]); ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'age')->textInput(['type' => 'number', 'max' => 120, 'min' => '18','id' => 'age']) ?>
        </div>


    </div>
    <?= $form->field($model, 'sex')->radioList(\app\models\User::getSexName())->label('Sex'); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'country')->widget(Select2::classname(), [
                'data' => $countries,
                'options' => ['placeholder' => 'choose country ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'city')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'city_type')->dropDownList(\app\models\User::getCityType()) ?>
        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
