<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <section id="section-1" class=" section">
        <div class="row text-justify">
            <h3 class="c_title_h3 text-center">Talk to Sales</h3>
            <h4 class="c_title_h4 text-center">Our experts love to answer questions.
                Fill out the form below.</h4>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                </div>
                <div class="col-md-2"></div>
            </div>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="here-to-help ">
                            <div class="here-to-help__header"><h4 class="text-black-h5">We’re here to help</h4>
                            </div>
                            <ul class="here-to-help__list">
                                <li>Find the right solution for you</li>
                                <li>Explain options for pricing</li>
                                <li>Connect you with helpful resources</li>
                            </ul>
                            <div class="helper-sections">
                                <div class="here-to-help__header"><p class="text-black-h5">Support</p></div>
                                <p class="here-to-help__body text-grey">Already a customer? If you are encountering a
                                    technical or payment issue, our customer support team will be happy to assist
                                    you.</p><a href="/help/contact" class="here-to-help__link text-pink">Contact Support</a>
                                <br>
                                <br>
                                <div class="here-to-help__header"><p class="text-black-h5">Report abuse</p></div>

                                <p class="here-to-help__body text-grey">Twilio is an SMS and voice provider, but we don’t send
                                    SMS messages or phone calls directly to end-users. Please report SMS or voice
                                    abuse to us, and we will be in touch.</p><a href="/help/abuse"
                                                                                class="here-to-help__link text-pink">Report a
                                    violation</a></div>
                        </div>
                    </div>
                    <div class="col-md-6" >
                        <div class="white_panel" data-ip-country="" data-ip-region="">
                            <div class="archetype-form__container archetype-form__container--form-1264" role="form">
                                <div class="form__fields ">
                                            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                                            <?= $form->field($model, 'first_name')->textInput(['autofocus' => true,'class' => 'form-control contact_input']) ?>

                                            <?= $form->field($model, 'last_name')->textInput(['class' => 'form-control contact_input']) ?>

                                            <?= $form->field($model, 'email')->textInput(['class' => 'form-control contact_input']) ?>

                                            <?= $form->field($model, 'company')->textInput(['class' => 'form-control contact_input']) ?>

                                            <?= $form->field($model, 'job')->textInput(['class' => 'form-control contact_input']) ?>

                                            <?= $form->field($model, 'phone')->textInput(['class' => 'form-control contact_input']) ?>

                                            <?= $form->field($model, 'country')->dropDownList(['America','Russian'],['class' => 'select-white-btn themes-select select-select select-theme-chosen']) ?>

<!---->
<!--                                            <div class="form-group">-->
<!--                                                <label for="first_name">First Name:</label>-->
<!--                                                <input type="text" name="first_name" class="form-control contact_input" id="first_name">-->
<!--                                            </div>-->
<!--                                            <div class="form-group">-->
<!--                                                <label for="last_name">Last Name:</label>-->
<!--                                                <input type="text" name="last_name" class="form-control contact_input" id="last_name">-->
<!--                                            </div>-->
<!--                                            <div class="form-group">-->
<!--                                                <label for="email">Email Address:</label>-->
<!--                                                <input type="email" name="email" class="form-control contact_input" id="email">-->
<!--                                            </div>-->
<!--                                            <div class="form-group">-->
<!--                                                <label for="company_name">Company Name:</label>-->
<!--                                                <input type="text" name="company_name" class="form-control contact_input" id="company_name">-->
<!--                                            </div>-->
<!--                                            <div class="form-group">-->
<!--                                                <label for="job">Job Title:</label>-->
<!--                                                <input type="text" name="job" class="form-control contact_input" id="job">-->
<!--                                            </div>-->
<!--                                            <div class="form-group">-->
<!--                                                <label for="phone_name">Phone Number:</label>-->
<!--                                                <input type="text" name="phone_name" class="form-control contact_input" id="phone_name">-->
<!--                                            </div>-->
<!--                                            <div class="form-group">-->
<!--                                                <label for="country">Country:</label>-->
<!--                                                <select style="width: 100%!important;" id="country" class="select-white-btn themes-select select-select select-theme-chosen">-->
<!--                                                    <option>Russian Federation</option>-->
<!--                                                    <option>USA</option>-->
<!--                                                    <option>China</option>-->
<!--                                                </select>-->
<!--                                            </div>-->

                                            <div class="form__group form__group--button text-center">
                                                <div class="form-group">
                                                    <?= Html::submitButton("Let's Talk", ['class' => 'btn-pink', 'name' => 'contact-button','style' => 'width: 273px; padding: 21px; font-size: 20px;']) ?>
                                                </div>
                                            </div>

                                         <br>

                                        <p class="form__terms text-grey">For information about how Twilio handles personal
                                            data, see our privacy statement at <a href="/legal/privacy" class="text-pink">Twilio
                                                Privacy</a>.</p>
                                        <div class="form-error" style="display: none;"></div>
                                    <?php ActiveForm::end(); ?>

                                </div>


                                <?php if( Yii::$app->session->hasFlash('success') ): ?>
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <?php echo Yii::$app->session->getFlash('success'); ?>
                                    </div>
                                <?php endif;?>



                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <br>

    <!--  Section Press review end -->
</div>