<?php
$this->title = 'Age Processor | Privacy Policy';

?>

<style>
    @media screen and (min-width: 760px) {
        .header_content {
            position: relative;
            height: 42px;
            top: 0px !important;
            max-width: 1316px;
            margin: 0 auto;
            padding: 0 60px;
        }

        .nav-block {
            box-shadow: 0px 4px 25px rgba(0, 0, 0, 0.05);
            padding: 20px;
        }
    }
    p{
        font-size: 20px;
        font-weight: 400;
        line-height: 35px;
    }
    h4{
        margin-bottom: 20px!important;
    }
</style>


<div id="fullpage">


    <!-- Footer Start -->
    <div class="container">
        <section id="section-1" class=" section">
            <div class="row text-justify">
                <h2 class="f_title_h3 text-center">PRIVACY POLICY</h2>
                <h4 class="text-black-h4"><b>Updated 7/11/2018</b></h4>
                <p>
                    As the parent company of AgeProcessor General Agency, LLC (“AgeProcessor”), GWG Holdings, Inc.
                    (collectively with AgeProcessor, “we,” “our,” or “us”) recognizes that you may have questions about
                    our collection, use and disclosure of information collected from you when you request a quote for
                    insurance and interact with our www.ageprocessor.com website (“Site”). We value our relationship with
                    you and want to help keep your experience on our Site secure, enjoyable and easy.

                </p>
                <br>
                <p>

                    This Privacy Policy summarizes our information collection practices and is provided with the goal of
                    helping you understand how we use this information.

                </p>
                <br>

                <h4><b> Contact</b></h4>
                <p> If you have any questions or comments about this Privacy Policy, please contact us at
                    info@ageprocessor.com.
                </p>
                <br>

                <h4><b>
                        Information we Collect
                    </b></h4>

                <h4><b>
                        Personally Identifiable Information (“PII”)
                    </b></h4>
                <p> We only collect personally identifiable information (“PII”), such as name, address, date of birth
                    telephone number, and e-mail address that you provide through our Site when you request an insurance
                    quote. If you have returned a Spit Kit to us and agreed to the terms of the Research Consent
                    Authorization Document, PII will also include Epigenetic, Genetic & Self-Reported Information (as
                    defined in the Consent Authorization Document).
                </p>
                <br>

                <h4><b> Non-Personally Identifiable Information (“NPI”)</b>
                </h4>
                <p>
                    We collect non-personal information (“NPI”). This information includes, but is not limited to, your
                    device ID, IP address, the type of operating system and browser that you use, your geo-location
                    data, and Site pages you visit. NPI cannot be easily used to personally identify you.

                </p>
                <br>

                <h4><b>
                        How we use PII
                    </b></h4>
                <p>
                    We use PII primarily for the purpose of responding and processing your request for a quote for
                    insurance coverage and, if you have received a Spit Kit and agreed to the terms of the Research
                    Consent Authorization Document, in connection with our epigenetic research. This use includes, but
                    is not limited to, sending your PII to one or more of our selected insurance carrier partners for
                    the purpose of preparing the insurance quote you requested and conducting our epigenetic research,
                    as applicable. We also use PII to process inquiries you may have regarding our products and
                    services, keep you informed about changes to our Site and to send you marketing materials from time
                    to time. If you would to be removed from our marketing list, you can opt out by sending an email to
                    info@ageprocessor.com.
                </p>
                <br>

                <h4><b> Use of NPI</b></h4>

                <p>
                    We use NPI to track your usage of the Site and other internal purposes, such as Site maintenance,
                    evaluation and enhancing the end-user experience, which includes displaying advertising based on
                    your Site visit patterns and other NPI data. If you would like to opt-out from our Site tracking,
                    please send an email to info@ageprocessor.com.

                </p>
                <br>

                <h4><b>
                        Disclosure of PII and NPI to Successors
                    </b></h4>

                <p>
                    We may disclose PII and NPI to any successor-in-interest of ours, such as, but not limited to, a
                    company that acquires us. In addition, if you have returned a Spit Kit to us and agreed to the terms
                    of the Research Consent Authorization Document, we may also disclose PII to the persons and
                    companies identified in the Research Consent Authorization Document.

                </p>
                <br>

                <h4><b>
                        Disclosure of PII and NPI in Compliance with Legal Requirements
                    </b></h4>

                <p>
                    We will disclose your PII and/or NPI whenever such disclosure is required by a court order,
                    government or regulatory request or by any other legal process.
                </p>
                <h4><b>
                        Security
                    </b></h4>
                <p>
                    We strive to maintain reasonable data security measures. However, no security measures are
                    invulnerable to attack. We are not responsible for the security of your PII that you transmit to us
                    over networks that we do not control, including, but not limited to, wireless networks.

                </p>
                <h4><b> Site Tracking Technologies</b></h4>
                <p>
                    We use various Site usage tracking technologies, including cookies, flash cookies and web beacons.
                    If you would like to change how your browser interacts with these tracking technologies, you should
                    consult your browser settings.
                </p>
                <h4><b> Information Relating to Third Party Websites</b></h4>

                <p>
                    Our Site may contain links to third party sites. We do not endorse nor otherwise accept
                    responsibility for the content or privacy policies of those sites. If you have any questions about
                    content delivered by or information tracked by those third party websites, you should contact the
                    third party directly.

                </p>
                <h4><b>
                        Changes to this Policy
                    </b></h4>
                <p>
                    We reserve the right to modify this Privacy Policy at any time and without prior notice. If we make
                    changes, we will post a notice of the change on the Site.
                </p>
            </div>
        </section>
        <br>

        <!--  Section Press review end -->
    </div>
    <!-- Footer End -->


