<?php
$this->title = 'Age Processor | Workspace';

?>
<style>
    @media screen and (min-width: 760px) {
        .header_content {
            position: relative;
            height: 42px;
            top: 0px !important;
            max-width: 1316px;
            margin: 0 auto;
            padding: 0 60px;
        }
        .nav-block {
            box-shadow: 0px 4px 25px rgba(0, 0, 0, 0.05);
            padding: 20px;
        }
    }

</style>
<div id="fullpage">
    <?php echo \frontend\widgets\ModalsWidget\ModalsWidget::widget(); ?>

    <!-- Footer Start -->
    <div class="container-fluid">
        <section id="section-1" class=" section">
            <div class="row padding-92" style="">

                <div class="col-md-1">

                </div>
                <div class="col-md-10 col-xs-12">
                    <h3 class="f_title_h3 mobile-none"></h3>
                    <div class="white_panel">
                        <div class="row" style="padding: 10px;">
                            <div class="col-md-3 col-xs-6">
                                <div id="progress1" data-value="50"></div>
                                <p class="text-center p_title">your chances to reach 70 y.o.</p>

                            </div>
                            <div class="col-md-3 col-xs-6">
                                <div id="progress2" data-value="50"></div>

                                <p class="text-center p_title">your chances to reach 80 y.o.</p>

                            </div>
                            <div class="col-md-3 col-xs-12 col-sm-6">
                                <p class="age-title"><span class="count">80</span> y.o</p>
                            </div>
                            <div class="col-md-3 col-xs-12 col-sm-6 p_title-24" style=""><p>
                                    most probable death age
                                </p></div>
                        </div>
                        <div class="col-md-1">

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="" class="section" style="margin-bottom: 30px;">
            <div class="holder ">
                <ul class="SteppedProgress Vertical" style="padding-bottom: 30px;">
                    <li class="complete active">
                        <div class="">
                            <div class="row margin-top" style="margin-bottom: 50px; ">
                                <div class="col-md-1 col-sm-1 col-xs-1 ">
                                </div>
                                <div class="col-md-10 col-sm-11 col-xs-12 " style="margin-top: 50px;">
                                    <h3 class="f_title_h3 margin-bottom">let’s work with accuracy</h3>
                                    <div class="row">
                                        <div class="col-md-4 col-xs-12 col-sm-6" >
                                            <div class="white_panel">
                                                <h4 class="text-black-h4"><b>Record voice</b> <span><img
                                                                src="/img/elements/surface1.svg"></span></h4>
                                                <p>our AI will analyze your voice to detect its frequency, tonality and other important parameters to compare with peers</p>
                                                <a href="#ex2" rel="modal:open">
                                                    <button class="btn-pink-mini">
                                                        <span class="pink-title-mini">Record voice</span>
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-6">
                                            <div class="white_panel">
                                                <h4 class="text-black-h4"><b>Take a photo</b> <span><img
                                                                src="/img/elements/surface1.svg"></span></h4>
                                                <p>we will analyse your photo and detect your relative face photo bioage</p>
                                                <a href="#ex1" rel="modal:open">
                                                    <button class="btn-pink-mini">
                                                        <span class="pink-title-mini">Take a photo</span>
                                                    </button>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xs-12 col-sm-6">
                                            <div class="white_panel">
                                                <h4 class="text-black-h4"><b>Write a test</b> <span><img
                                                                src="/img/elements/surface1.svg"></span></h4>
                                                <p>Qucik survey will provide us with wide data to analyze most important information
                                                about inhertited and obtained conditions</p>
                                                <a href="#ex1" rel="modal:open">
                                                    <button class="btn-pink-mini">
                                                        <span class="pink-title-mini">Write a test</span>
                                                    </button>
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </li>
                    <li class="">
                        <h3 class="text-center text-black-h3" style="position: relative;     background: white;
    z-index: 9999;"><spanmi
                                    style="inline-size: 30px;color:silver; "></spanmi> Submit
                            at least 2 points above to unlock more <span
                                    style="color:silver"> </span></h3>
                        <div class="row blur-me" style="margin-top: 50px;  ">
                            <div class="col-md-1 col-sm-1 col-xs-1">
                            </div>
                            <div class="col-md-10 col-sm-11 col-xs-12">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12 col-sm-6">
                                        <div class="white_panel">
                                            <h4 class="text-black-h4"><b>Lab analysis <span><img
                                                                src="/img/elements/surface1.svg"></span></b></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et congue
                                                netus tellus
                                                augue aenean gravida tempus.</p>
                                            <a href="workspace.html">
                                                <button class="btn-pink-mini">
                                                    <span class="pink-title-mini">Take a photo</span>
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-6">
                                        <div class="white_panel">
                                            <h4 class="text-black-h4"><b>Relatives</b> <span><img
                                                            src="/img/elements/surface1.svg"></span></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et congue
                                                netus tellus
                                                augue aenean gravida tempus.</p>
                                            <a href="workspace.html">
                                                <button class="btn-pink-mini">
                                                    <span class="pink-title-mini">Record voice</span>
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-6">
                                        <div class="white_panel">
                                            <h4 class="text-black-h4"><b>Statistics</b> <span><img
                                                            src="/img/elements/surface1.svg"></span></h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et congue
                                                netus tellus
                                                augue aenean gravida tempus.</p>
                                            <a href="workspace.html">
                                                <button class="btn-pink-mini">
                                                    <span class="pink-title-mini">Write a test</span>
                                                </button>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="blur-me">
                        <div class="">
                            <div class="row" style="margin-bottom: 50px; ">
                                <div class="col-md-1 col-sm-2 col-xs-1">
                                </div>
                                <div class="col-md-10 col-sm-9 col-xs-12">
                                    <h3 class="f_title_h3">Recomendations</h3>
                                    <div class="row">
                                        <div class="col-md-6 col-xs-12 col-sm-6">
                                            <div class="card-2">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-12 col-xs-12"
                                                         style="margin-right: 37px">
                                                        <img class="card2-img" src="/img/elements/card_photo.png"
                                                             width="200px">
                                                    </div>
                                                    <div class="col-md-6  card2-content" style="">
                                                        <h4 class="text-black-h3"><b>Motion</b></h4>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                                                            et congue netus tellus
                                                            augue aenean gravida tempus.</p>
                                                        <a class="text-pink">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-sm-6">
                                            <div class="card-2">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-12 col-xs-12"
                                                         style="margin-right: 37px">
                                                        <img class="card2-img" src="/img/elements/card_photo.png"
                                                             width="200px">
                                                    </div>
                                                    <div class="col-md-6 card2-content" style="">
                                                        <h4 class="text-black-h3"><b>Motion</b></h4>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                                                            et congue netus tellus
                                                            augue aenean gravida tempus.</p>
                                                        <a class="text-pink">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-6 col-xs-12 col-sm-6">
                                            <div class="card-2">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-12 col-xs-12"
                                                         style="margin-right: 37px">
                                                        <img class="card2-img" src="/img/elements/card_photo.png"
                                                             width="200px">
                                                    </div>
                                                    <div class="col-md-6 card2-content" style="">
                                                        <h4 class="text-black-h3"><b>Motion</b></h4>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                                                            et congue netus tellus
                                                            augue aenean gravida tempus.</p>
                                                        <a class="text-pink">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-sm-6">
                                            <div class="card-2">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-12 col-xs-12"
                                                         style="margin-right: 37px">
                                                        <img class="card2-img" src="/img/elements/card_photo.png"
                                                             width="200px">
                                                    </div>
                                                    <div class="col-md-6 card2-content" style="">
                                                        <h4 class="text-black-h3"><b>Motion</b></h4>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                                                            et congue netus tellus
                                                            augue aenean gravida tempus.</p>
                                                        <a class="text-pink">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <br>

                                    <div class="row">
                                        <div class="col-md-6 col-xs-12 col-sm-6">
                                            <div class="card-2">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-12 col-xs-12"
                                                         style="margin-right: 37px">
                                                        <img class="card2-img" src="/img/elements/card_photo.png"
                                                             width="200px">
                                                    </div>
                                                    <div class="col-md-6 card2-content" style="">
                                                        <h4 class="text-black-h3"><b>Motion</b></h4>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                                                            et congue netus tellus
                                                            augue aenean gravida tempus.</p>
                                                        <a class="text-pink">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-xs-12 col-sm-6">
                                            <div class="card-2">
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-12 col-xs-12"
                                                         style="margin-right: 37px">
                                                        <img class="card2-img" src="/img/elements/card_photo.png"
                                                             width="200px">
                                                    </div>
                                                    <div class="col-md-6 card2-content" style="">
                                                        <h4 class="text-black-h3"><b>Motion</b></h4>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                                                            et congue netus tellus
                                                            augue aenean gravida tempus.</p>
                                                        <a class="text-pink">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>

            </div>
        </section>

        <!--  Section Our Team  -->
        <section id="section-4" class="section">

        </section>
        <!--  Section Press review end -->
    </div>
    <!-- Footer End -->

</div>
<div id="myModal" class="modal">
    <span class="close glyphicon glyphicon-arrow-left"></span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
</div>
<!-- Footer End -->
<script>


</script>
