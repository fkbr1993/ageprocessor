<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\DataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Add data';
?>
<div class="data-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?php  echo $this->render('_form', ['model' => $model]); ?>

    <table class="table ">
        <tr>
            <td>№</td>
            <td>File</td>
        </tr>
        <?php foreach ($files as $key => $file):?>
            <tr>
                <td><?=$key?></td>
                <td><a href="<?=Yii::getAlias('@web').'/uploads/'.$file->file?>" download=""><?=$file->file?></a></td>
                <td style="width: 100px;">
                    <a href="<?=Yii::getAlias('@web').'/uploads/'.$file->file?>"  download="" class="btn btn-primary"><i class="glyphicon glyphicon-download"></i></a>
                    <a href="delete-file?id=<?=$file->id?>" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
                </td>
            </tr>
        <?php endforeach;?>

    </table>


</div>
