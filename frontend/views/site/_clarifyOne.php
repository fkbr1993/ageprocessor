<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \frontend\models\forms\AnalyzeForm */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile(   '/js/RecordRTC.js');
$this->registerJsFile(   '/js/waveform.js');

?>
<h2>Clarify your results</h2>

<div class="arrow">
    <svg width="75" height="64" viewBox="0 0 75 64" fill="none" xmlns="http://www.w3.org/2000/svg">
        <rect x="16" width="44" height="33" fill="#2E8C6A"/>
        <path d="M37.5 64L74.3061 32.5H0.69392L37.5 64Z" fill="#2E8C6A"/>
    </svg>
</div>



<div class="data-form row-options">

   <div class="column-option">
        Record a voice
        <br>
        <img class="option-icon record" id="btn-start-recording" src="/images/baseline-record_voice_over-24px.svg">
        <img class="option-icon blinker" id="btn-stop-recording" src="/images/baseline-stop-24px.svg">
       <button id="btn-release-microphone" disabled>Release Microphone</button>

       <div style="display: none;"><audio controls autoplay playsinline></audio></div>

   </div>

    <div class="column-option">
        Take a photo
        <br>
        <img class="option-icon" src="/images/baseline-camera_enhance-24px.svg">
    </div>

    <div class="column-option">
        Pass a test
        <br>
        <img class="option-icon" src="/images/baseline-insert_chart_outlined-24px.svg">
    </div>

</div>
