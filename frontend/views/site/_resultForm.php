<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \frontend\models\forms\AnalyzeForm */
/* @var $form yii\widgets\ActiveForm */

?>
<h1 class="not-so-lead">
    Analyze your life expectancy
</h1>

<div class="data-form">

    <?php $form = ActiveForm::begin(['method' => 'GET', 'action' => '/']); ?>

        <?= $form->field($model, 'age')->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'sex')->hiddenInput()->label(false); ?>
        <?= $form->field($model, 'country')->hiddenInput()->label(false); ?>

        <?= Html::submitButton(
            '&#8592; Back to data',
            ['class' => 'btn btn-success btn-back']
        ) ?>

        <div class="form-group field-analyzeform-age required">
            <div class="form-control-div">
                <label class="control-label" for="analyzeform-age">Chance to survive 12 month</label>
                <div class="the-digits">
                    <?php $res = $model->getResults()['12']; ?>
                    <?php $valDig = (int)$res; ?>
                    <?php $valDec = 100*((float)$res-floor($res)); ?>
                    <span class="spmdigs" data-val="<?= $valDig ?>">0</span>.<span class="spmdecs" data-val="<?= $valDec?>">0</span>%
                </div>
            </div>
        </div>
        <div class="form-group field-analyzeform-sex required">
            <div class="form-control-div">
                <label class="control-label" for="analyzeform-sex">Chance to reach <?= $model->getResults()['to'][0] ?> years</label>
                <div class="the-digits">
                    <?php $res = $model->getResults()['to'][1]; ?>
                    <?php $valDig = (int)$res; ?>
                    <?php $valDec = 100*((float)$res-floor($res)); ?>
                    <span class="spmdigs" data-val="<?= $valDig ?>">0</span>.<span class="spmdecs" data-val="<?= $valDec?>">0</span>%
                </div>
            </div>
        </div>
        <div class="form-group field-analyzeform-country required">
            <div class="form-control-div">
                <label class="control-label" for="analyzeform-country">Life expectancy</label>
                <div class="the-digits">
                    <?php $res = $model->getResults()['most']; ?>
                    <?php $valDig = (int)$res; ?>
                    <span class="spmdigs" data-val="<?= $valDig ?>">0</span>y.
                </div>
            </div>
        </div>


    <?php ActiveForm::end(); ?>

</div>
