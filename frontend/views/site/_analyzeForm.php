<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \frontend\models\forms\AnalyzeForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-form">

    <?php $form = ActiveForm::begin(['method' => 'GET', 'action' => '/site/result']); ?>

        <?= $form->field($model, 'age')->input('number'); ?>
        <?= $form->field($model, 'sex')->dropDownList(['1' => 'Male', '2' => 'Female']); ?>
        <?= $form->field($model, 'country')->dropDownList([ 1=> 'Russia', 2 => 'USA', 3 => 'UK']); ?>

        <?= Html::submitButton(
            'Start analysis ><span class="blinker">_</span>',
            ['class' => 'btn btn-success', ]
            //onclick="__gaTracker('send', 'event', 'buttons', 'click', 'phone-number-middle');"
        ) ?>

    <?php ActiveForm::end(); ?>

</div>
