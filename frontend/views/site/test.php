<?php

/* @var $this yii\web\View */
/* @var $model \frontend\models\forms\AnalyzeForm */


$this->title = 'Lifetracker — first lifespan enhancing service';

use yii\authclient\widgets\AuthChoice;
use yii\web\View;
$this->registerJsFile(   '/js/RecordRTC.js');
$this->registerJsFile(   '/js/waveform.js');



/*
$path = __DIR__. "/../../../RecordRTC/RecordRTC-to-PHP/index.php";

include $path;*/

?>

<button id="btn-start-recording">Start Recording</button>
<button id="btn-stop-recording" disabled>Stop Recording</button>
<button id="btn-release-microphone" disabled>Release Microphone</button>

<div><audio controls autoplay playsinline></audio></div>

