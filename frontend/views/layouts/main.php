<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:ageprocessor.com" content="ageprocessor.com">
    <meta property="og:Age Processor" content="Age Processor">
    <meta name="yandex-verification" content="7a0cb79ae3b89180" />
    <link href="/favicon.png" rel="icon" type="image/x-icon">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-153636318-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-153636318-1');
    </script>

</head>
<body>
<?php $this->beginBody() ?>


<!--   --><?php
//    NavBar::begin([
//        'brandLabel' => '</span>Life<span class="tracker">tracker</span>',
//        'brandUrl' => Yii::$app->homeUrl,
//        'options' => [
//            'class' => 'navbar-inverse navbar-fixed-top',
//        ],
//    ]);
//    $menuItems = [
//        ['label' => 'Home', 'url' => ['/site/index']],
//        ['label' => 'About', 'url' => ['/site/about']],
//        ['label' => 'Contact', 'url' => ['/site/contact']],
//    ];
//    if (Yii::$app->user->isGuest) {
//        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
//        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
//    } else {
//        $menuItems[] = ['label' => 'Profile', 'url' => ['/site/profile']];
//        $menuItems[] = ['label' => 'Data', 'url' => ['/site/add-data']];
//        $menuItems[] = '<li>'
//            . Html::beginForm(['/site/logout'], 'post')
//            . Html::submitButton(
//                'Logout (' . Yii::$app->user->identity->email . ')',
//                ['class' => 'btn btn-link logout']
//            )
//            . Html::endForm()
//            . '</li>';
//    }
//    echo Nav::widget([
//        'options' => ['class' => 'navbar-nav navbar-right'],
//        'items' => $menuItems,
//    ]);
//    NavBar::end();
//    ?>
<nav class="navbar center-block nav-block">
    <div class=" header_content">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="navMobile">
                <span class="" style="color:black"> <img style="    margin-top: 12px;" src="/img/elements/bars.svg"></span>

            </button>
            <a class="navbar-brand header_logo" href="/">   <img src="/img/logo.svg">
            </a>
        </div>
        <div class="collapse navbar-collapse menu" id="navMobile" style="height: 0">
            <ul class="nav navbar-nav navbar-right ">
                <li><a href="/site/contact">Contact us</a></li>
                <li><a href="#">Help</a></li>
                <li><a href="/site/privacy-policy">Privacy Policy</a></li>
            </ul>

        </div>
    </div>
</nav>
<!--        --><?//= Alert::widget() ?>
        <?= $content ?>

<!--Bottom-->
<div class="bottom_content">
    <div class="menu_bottom" style="margin-top: 100px;">
        <div class="header_logo" style="    margin-top: 15px">
            <a href="#">
                <img class="desctop_logo" src="/img/logo.svg">
                <img class="table_logo" src="/img/Mini.svg">
            </a>
        </div>
        <div class="menu menu-bottom" style="">
            <ul>
                <li><a href="#">Contact us</a></li>
                <li><a href="#">Help</a></li>
                <li><a href="#">Privacy Policy</a></li>
            </ul>
        </div>
    </div>
</div>

<!--Bottom end -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
