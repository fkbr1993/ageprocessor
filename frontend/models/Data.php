<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "data".
 *
 * @property int $id
 * @property int $user_id
 * @property int $partner_id
 * @property string $raw_data
 * @property string $file
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $user
 */
class Data extends \yii\db\ActiveRecord
{
    public $uploadFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'partner_id', 'created_at', 'updated_at'], 'integer'],
            [['raw_data'], 'string'],
            [['file'], 'string', 'max' => 255],
            [['uploadFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'xls, xml , sql , xlsx'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'partner_id' => 'Partner ID',
            'raw_data' => 'Raw Data',
            'file' => 'File',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
  
}
