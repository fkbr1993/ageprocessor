<?php
namespace frontend\models\forms;

use app\models\LtData;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\VarDumper;

/**
 * ContactForm is the model behind the contact form.
 */
class AnalyzeForm extends Model
{
    public $age;
    public $sex;
    public $country;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['age', 'sex', 'country', ], 'required'],
            // email has to be a valid email address
            [['age', 'sex', 'country'],'integer'],
            [['age', ],'number', 'min' => 1, 'max' => 99,],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'age' => 'Age',
            'sex' => 'Sex',
            'country' => 'Country',
        ];
    }

    public function getResults()
    {
        if ($this->age && $this->sex && $this->country){
            $lt = LtData::findOne(['age' => $this->age, 'sex' => $this->sex, 'country' => $this->country]);
            $to = 70;
            if ($this->age >= 65){
                $to = 80;
            }
            if ($this->age >= 75){
                $to = 90;
            }
            if ($this->age >= 85){
                $to = $this->age+5;
            }

            $sum = '17.13'; //1-sum(Data[qx][Age],Data[qx][AvgExp]) = p1 веро€тность дожить до AvgExp
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
                SELECT SUM(qx) AS sum
                FROM lt_data
                WHERE country = :country AND sex = :sex AND age >= :age AND age <= :to",
                [
                    ':country' => $this->country, ':age' => $this->age, ':sex' =>$this->sex, ':to' => $to,
                ]);
            $result = $command->queryAll();
            $sum = round(1-$result[0]['sum'], 4)*100;

            return [
                '12' => round(1-$lt->qx, 4)*100,
                'to' => [$to, $sum],
                'most' => $this->age + round($lt->ex),
            ];
        }
        return [
            '12' => 95.55,
            'to' => [70, 17.13],
            'most' => 63,
        ];
    }
}