<?php
namespace frontend\models\forms;

use app\models\LtData;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\VarDumper;

/**
 * ContactForm is the model behind the contact form.
 */
class VoiceForm extends Model
{
    /**
     * @var UploadedFile|Null file attribute
     */
    public $file;
    public $filename;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['filename'], 'string'],
            [['file'], 'file'],
        ];
    }
}