<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "data".
 *
 * @property int $id
 * @property int $country
 * @property int $sex
 * @property int $ex
 * @property int $qx
 *
 * @property User $user
 */
class LtData extends \yii\db\ActiveRecord
{
    public $uploadFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lt_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
    }
  
}
