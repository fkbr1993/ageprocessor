import argparse
import random


def write_str_to_file(str_to_write, filename):
    with open(filename, "w") as f:
        f.writelines([str_to_write])



parser = argparse.ArgumentParser()
parser.add_argument('-i', '--input', help="input wav")
parser.add_argument('-a', '--algorithm', help="[age|sex]")


args = parser.parse_args()

#print(f"args = {args}")
#print(args.input)
#print(args.algorithm)

result = None
if args.algorithm == "age":
    result = random.randrange(15, 60)
else:
    result = random.randrange(0, 2)

print(result)
write_str_to_file(str(result), "result.txt")

