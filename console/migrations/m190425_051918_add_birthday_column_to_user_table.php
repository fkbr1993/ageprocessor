<?php

use yii\db\Migration;

/**
 * Handles adding birthday to table `{{%user}}`.
 */
class m190425_051918_add_birthday_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'birthday', $this->date());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'birthday');
    }
}
