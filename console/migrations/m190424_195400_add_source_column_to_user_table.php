<?php

use yii\db\Migration;

/**
 * Handles adding source to table `{{%user}}`.
 */
class m190424_195400_add_source_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'source', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'source');
    }
}
