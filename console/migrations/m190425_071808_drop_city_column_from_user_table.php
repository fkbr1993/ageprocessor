<?php

use yii\db\Migration;

/**
 * Handles dropping city from table `{{%user}}`.
 */
class m190425_071808_drop_city_column_from_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%user}}', 'city');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%user}}', 'city', $this->integer());
    }
}
