<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%lt_data}}`.
 */
class m190605_225207_create_lt_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%lt_data}}', [
            'id' => $this->primaryKey(),
            'country' => $this->integer()->notNull(),
            'sex' => $this->integer()->notNull(),
            'age' => $this->integer()->notNull(),
            'mx' => $this->float()->notNull(),
            'qx' => $this->float()->notNull(),
            'lx' => $this->float()->notNull(),
            'lbx' => $this->float()->notNull(),
            'ex' => $this->float()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lt_data}}');
    }
}
