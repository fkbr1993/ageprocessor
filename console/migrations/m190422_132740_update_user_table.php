<?php

use yii\db\Migration;

/**
 * Class m190422_132740_update_user_table
 */
class m190422_132740_update_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'age', $this->float());
        $this->addColumn('{{%user}}', 'country', $this->integer());
        $this->addColumn('{{%user}}', 'sex', $this->integer());
        $this->addColumn('{{%user}}', 'city', $this->integer());
        $this->addColumn('{{%user}}', 'city_type', $this->integer());
        $this->addColumn('{{%user}}', 'first_name', $this->string());
        $this->addColumn('{{%user}}', 'middle_name', $this->string());
        $this->addColumn('{{%user}}', 'last_name', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'age');
        $this->dropColumn('{{%user}}', 'country');
        $this->dropColumn('{{%user}}', 'sex');
        $this->dropColumn('{{%user}}', 'city');
        $this->dropColumn('{{%user}}', 'city_type');
        $this->dropColumn('{{%user}}', 'first_name');
        $this->dropColumn('{{%user}}', 'middle_name');
        $this->dropColumn('{{%user}}', 'last_name');
    }
}
