<?php

use yii\db\Migration;

/**
 * Class m190422_182434_update_user_table
 */
class m190422_182434_update_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%user}}', 'username');
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->addColumn('{{%user}}', 'username', 'string');
    }
}
