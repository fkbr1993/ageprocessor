<?php

use yii\db\Migration;

/**
 * Class m190605_225218_insert_into_lt_data_table
 */
class m190605_225219_insert_into_lt_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $lines = file(__DIR__ . '/' . 'lifetable_2016_gb.txt');
        foreach ($lines as $age => $string){
            $line = explode(' ', $string);
            $this->insert('{{%lt_data}}',[
                 'country' => 3,
                 'sex' => 1,
                 'age' => 1*$line[0],
                 'mx' => (float)str_replace(',', '.', $line[1]),
                 'qx' => (float)str_replace(',', '.', $line[2]),
                 'lx' => (float)str_replace(',', '.', $line[3]),
                 'lbx' => (float)str_replace(',', '.', $line[4]),
                 'ex' => (float)str_replace(',', '.', $line[5]),
            ]);
            $this->insert('{{%lt_data}}',[
                'country' => 3,
                'sex' => 2,
                'age' => 1*$line[0],
                'mx' => (float)str_replace(',', '.', $line[6]),
                'qx' => (float)str_replace(',', '.', $line[7]),
                'lx' => (float)str_replace(',', '.', $line[8]),
                'lbx' => (float)str_replace(',', '.', $line[9]),
                'ex' => (float)str_replace(',', '.', $line[10]),
            ]);
        };
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }

}
