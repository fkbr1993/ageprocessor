<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%partner_data}}`.
 */
class m190422_131149_create_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%data}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'partner_id' => $this->integer(), // if this field is empty, then this is a test
            'raw_data' => $this->text(),
            'file' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(
            'data_to_user_fk',
            'data',
            'user_id',
            '{{user}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'data_to_user_fk',
            'data'
        );

        $this->dropTable('{{%data}}');
    }
}
